# Docker Swarm auf einem RaspberryPi Cluster

Diese Dokumentation enthält Anweisungen und Tools für den Aufbau eines
Docker Swarms auf RaspberryPis, verwaltet mit Ansible.  
Verwendete Docker Images sind auf unserem [DockerHub](https://hub.docker.com/u/uklft/) zu finden.  


![Docker](https://gitlab.com/flopsters/PiHarbor/raw/master/docs/swarm.png)

------


## Getting started
### TL;DR

Ausgehend von __Ubuntu 14.04 <__ und drei RaspberryPis über Ethernet verbunden:

```bash
# Abhängigkeiten installieren
sudo apt-get install -y pv curl python3-pip python3-setuptools unzip hdparm git
sudo pip3 install ansible

# Repository klonen
git clone git@gitlab.com:flopsters/PiHarbor.git
cd PiHarbor/ansible

# Aktuelles hypriotOS Image herunterladen
wget https://github.com/hypriot/image-builder-rpi/releases/download/v1.9.0/hypriotos-rpi-v1.9.0.img.zip

# Skript zum flashen der Speicherkarten ausführen
bash scripts/flash_ansible_nodes.sh hypriotos-rpi-v1.9.0.img.zip
```

Wir geben nun manuell das SD-Karten Laufwerk an (meistens `mmcblk0`) und bestätigen das Gerät.  
Nach dem Flashen werden die RaspberryPis gestartet. Wir warten bis Diese online sind, dann können wir das Ansible Playbook zum initialisieren des Swarms ausführen:

```
ansible-playbook tasks/swarm.yaml
```


## Im Detail
### Schritte
* Ansible installieren
* Repository klonen
* HypriotOS-Image herunterladen
* Ansible `inventory` konfigurieren
* Flash-Skript verwenden
  + Hostnamen und Schnittstellen aus dem `inventory` generieren
  + Wifi-Zugangsdaten auslesen (falls konfiguriert)
  + Flashen der Images + Konfigurationen mit `flash.sh`
* Booten der RaspberryPis
* Ansible Tasks Ausführen
* Fertig!


### Ansible installieren
Für die Arbeit benötigen wir `python3` und den Python-Paketmanager `pip`.
Pip funktioniert wie apt/yum/pacman, nur für Python-Anwendungen.

Auf dem Commander[^XX]-Computer:
```bash
sudo apt-get install -y pv curl python3-pip python3-setuptools unzip hdparm git
sudo pip3 install ansible
```

### Klonen des Repository
Zuerst wird der Projektordner in unser aktuelles Verzeichnis heruntergeladen.  
Von dort aus werden wir schließlich anfangen, Befehle an Ansible zu übergeben.

```bash
git clone git@gitlab.com:flopsters/PiHarbor.git
cd PiHarbor/ansible
```

Dann laden wir das neueste HypriotOS herunter.  
Die aktuellsten Images sind auf der Release-Seite zu finden:    
https://github.com/hypriot/image-builder-rpi/releases/

```bash
wget https://github.com/hypriot/image-builder-rpi/releases/download/v1.9.0/hypriotos-rpi-v1.9.0.img.zip
```

#### (Optional) Privater Schlüssel für Ansible
Wir erzeugen ein privat-öffentliches SSH-Schlüsselpaar und konfigurieren dann Ansible, diesen als Standardschlüssel zu verwenden. Dies geschieht durch das Setzen einer Variablen in der Ansible Konfigurationsdatei `ansible.cfg`:

```bash
ssh-keygen -t rsa -b 4096 -C "$USER@$HOSTNAME" -f ./key/ansible
echo "private_key_file = ./keys/ansible" >> ansible.cfg
```

#### (Optional) Wifi-Anmeldeinformationen

```bash
$ vim inventory
(hypriotOS)
rpi0 swarm_iface=eth1
rpi1
rpi2 swarm_iface=wlan0
```

Um HypriotOS-Hosts mit Wifi-Anmeldeinformationen zu flashen,
bearbeiten wir die Variable `swarm_iface` im `inventory`.
Diese wird später von Ansible verwendet, um die Einrichtung von Wifi und Docker zu automatisieren.

"rpiX" ist der Standardname für unsere RaspberyPis, wir können stattdessen auch einen beliebigen String verwenden, Sonderzeichen (§ä¬ø -!...) gilt es allerdings zu vermeiden!.

Wenn `flash_ansible_nodes.sh` ausgeführt wird, fragt das Skript bei Ansible nach den Werten `rpi_wifi_pw` und `rpi_wifi_ssid` in der Datei `group_vars/hypriotOS.yaml`. Wir empfehlen stehts in Betracht zu ziehen, diese Datei mit [ansible_vault](http://docs.ansible.com/ansible/2.4/vault.html) zu verschlüsseln.

```bash
echo "rpi_wifi_ssid: Ihre-Wifi-SSID" >> group_vars/hypriotOS.yaml
echo "rpi_wifi_pw: dein-wifi-passwort" >> group_vars/hypriotOS.yaml

Geben Sie Ihr neues supersicheres Passwort für verschlüsselte Variablen ein.
ansible-vault encrypt group_vars/hypriotOS.yaml
```

### Flash & Reboot
Das Skript `flash_ansible_nodes` erledigt folgende Aufgaben für uns:
* Suchen nach `flash.sh` auf dem System für das Flashen der Pis
* Ansible-Task ausführen, der eine `user-data.yaml`-Datei für jeden Node unter `/tmp/rpiX.yaml` erstellt.
  + Die Datei `user-data.yaml` enthält die Konfiguration, die das HypriotOS beim Booten anwenden wird. Der [HypriotOS-Blogpost ](https://blog.hypriot.com/post/cloud-init-cloud-on-hypriot-x64/)  und unser Jinja2-[template](tasks/user-data.j2) zeigen, wie Ansible und HypriotOS hier perfekt ineinander greifen.
  + Wenn in `group_vars` Wifi-Anmeldedaten gesetzt sind, werden sie in die Konfigurationsdatei gerendert.
* Schleife über alle `Inventar`-Hosts:
  + Fragen, ob geflash werden soll
* Wenn ja, führe `flash.sh` mit der entsprechenden `user-data.yaml` und dem HypriotOS-Image aus.

Hier beginnt wieder der manuelle Teil.  

Der Flashing-Prozess wurde ausdrücklich __nicht__ automatisiert, da Kopieren von Dateisystemabbildern auf Raw-Devices (`/dev/*`) eine riskante Sache ist.  
BenutzerInnen sollten hier die __vollständige__ Kontrolle darüber haben.

So starten wir nun das Skript (`bash scripts/flask_ansible_nodes.sh`),
geben manuell unser SD-Karten Laufwerk ein (meistens `/dev/mmcblk0`) und bestätigen mögliche Authorisierungen.  
Nach dem Flashen werden die RaspberryPis gestartet, und wir warten, bis alle online sind.
Wer möchte kann die Wartezeit damit überbrücken, mit diesem unlesbaren Bash-Einzeiler die RaspberryPis zu Pingen um deren Status zu überprüfen:

```bash
while true;do clear;for i in rpi{0..3};do ping -c 1 $i;done;sleep 2;done
```


### Initialisieren des Docker Swarms
Für diesen Schritt verwenden wir ein einfaches, aber intelligentes Playbook, dass auf   
[GitHub](https://github.com/nextrevision/ansible-swarm-playbook) gefunden werden kann.  

Dieses Playbook geht davon aus, dass ein Docker-Daemon auf den Hosts läuft und die Gruppen _manager_ und _worker_ im Ansible `inventory` existieren.

Dies wurde in der beiliegenden `Inventardatei` berücksichtigt.   
Da wir HypriotOS verwenden, ist eine aktuelle Dockerversion vorinstalliert.  
Es kann also sofort los gehen:

~~~bash
ansible-playbook tasks/swarm.yml
~~~

### (Optional) Testen wir den Schwarm.
Zu diesem Zweck wurde ansibles Playbook zur Verfügung gestellt, das eine Instanz von __portainer__ auf jedem Manager-Knoten startet. Dies gibt uns ein schönes Webinterface um den Swarm zu verwalten und einige Infos darüber, was der Cluster macht.

~~~bash
ansible-playbook tasks/portainer.yaml
~~~


## Weiteres Management
### Einen Knoten hinzufügen
* Hostnamen und optionale Angaben werden im `Inventar` hinzugefügt.
* Das Flash-Skript wird dann erneut ausgeführt.
* Werden wir aufgefordert, die Knoten zu flashen, drücken wir in diesem Fall "_N_", bis der neue Knoten abgefragt wird.
* Jetzt lassen wir das Ansible Swarm Playbook noch einmal laufen


### Einen Knoten entfernen
Der zu entfernenden Knoten wird der Gruppe `setup` in unserem `Inventar` hinzugefügt.

Notiz: Wenn du nicht möchtest, dass der entfernte Knoten wieder dem Schwarm hinzugefügt wird, denke daran, ihn aus der manager/worker-Gruppe im `Inventar` zu entfernen!



### Sicherheit
#### Ansible Vault
Ansible kennt __verschlüsselte yaml__ Dateien, so genannte __vaults__.
Mit dem Befehl `ansible-vault` können wir diese erstellen, ansehen oder ändern:

```
ansible-vault encrypt group_vars/hypriotOS.yaml
# oder
ansible-vault create group_vars/new_group.yaml
# oder
ansible-vault edit group_vars/hypriotOS.yaml
```

In unserem Projektordner `ansible/` sollten die Befehle einen Befehlszeilen-Text-Editor ( nano/vim) öffnen. Hier können Variablen hinzugefügt oder geändert werden.
Wenn die Vault-Dateien unter `ansible/group_vars` liegen, sind sie automatisch mit den `Inventory`-Gruppen verlinkt, und können in `Playbooks` verwendet werden:

```
- hosts: swarm_manager_bootstrap[0]
   tasks:
     - name: initialize swarm cluster
       shell: docker swarm init --advertise-addr={{ swarm_iface | default('eth0') }}:2377

```

Um die Passphrase von Vault-Dateien zu ändern, verwenden wir `ansible-vault rekey`:
```ansible-vault rekey group_vars/hypriotOS.yaml
```


## Einen Testdienst erstellen
Um den Cluster zu testen und einen schnellen Überblick zu bekommen, was auf ihm passiert, starten wir einen Dockerdienst, der auf einem Container namens Visualizer basiert.

![Schwarm Visualizer](docs/viz.png)

````bash
sudo docker service create \
        --name viz \
        --publish 8080:8080/tcp \
        --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock \
        alexellis2/visualizer-arm:zuletzt
```
