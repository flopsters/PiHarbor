#!/bin/bash

# flash_ansible_hosts.sh
#
# This script is intended to automate the steps of flashing
# os images (especially HypriotOS) to sdcards.
# Therefor it reads the hosts and details from the local ansible environment.
#
# Steps:
# * Look for the flashing script
# * Get list of hosts in group _hypriotOS_ via ansible command
# * Create the final flash command for each

if [ -n `which flash` ]; then
  echo
  echo Found 'flash' installed on the system. Using this.
  flash_location=$(which flash)
else
  echo "No flash.sh found! Using contained"
  flash_location=$(readlink -f scripts/flash.sh)
fi


# Use ansible to list all hosts in hypriotOS category
hosts=$(ansible hypriotOS --list-hosts 2>/dev/null | grep -v hosts|sed "s/\w+//g")
echo "Found hosts:"
for i in $hosts; do echo $i; done

echo "Generating user-data.yaml files for all hosts"
ansible-playbook tasks/rpi-user-data.yaml --ask-vault-pass

for host in $hosts; do
  # Ensure the host should be flashed
  echo "INFO: Flash $host? (y/N)"
  read flash_host

  if [ "$flash_host" != "y" ]; then
    echo "Skipping..."
    continue
  fi

  # Collecting info for the flash command
  flash_str="sudo $flash_location \
        --userdata /tmp/$host-user-data.yaml $1"

  # ask user to repeat the flashing process for each host,
  # as this tends to break sometimes
  try_again="y"
  while [ "$try_again" == "y" ]; do
    echo ":"
    echo "Command:"
    echo $flash_str
    eval $flash_str
    echo "Try again if not successful? (y/N)"
    read try_again
  done
done
