#!/bin/bash
# ssh-multi.sh - a script to ssh multiple servers over multiple tmux panes
# usage: type tmux then from inside tmux type ssh-multi.sh HOST1 HOST2 ... HOSTN
# Muayyad Alsadi, D.Kovalov
# https://gist.github.com/muayyad-alsadi/bd25845776bb6b4185ba/
# https://gist.github.com/dmytro/3984680
# Based on http://linuxpixies.blogspot.jp/2011/06/tmux-copy-mode-and-how-to-control.html
# Tested with Ubuntu 16.04 and tmux 2.1

function error() {
  echo "$@" >&2
  exit -1
}

starttmux() {
    local hosts=( $HOSTS )

    first="ssh ${hosts[0]}"
    unset hosts[0]

    for i in "${hosts[@]}"; do
        tmux split-window -h  "ssh $i"
        tmux select-layout even-horizontal >/dev/null
        #tmux select-layout tiled > /dev/null
    done
    tmux select-pane -t 0
    tmux set-window-option synchronize-panes on > /dev/null

    $first
}

[ $# -lt 1 ] && error "usage: $0 HOST1 HOST2 ..."

HOSTS=$@

tmux refresh -S 2>/dev/null && starttmux "$@" || error "please run from inside tmux"
