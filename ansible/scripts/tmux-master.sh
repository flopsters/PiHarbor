#!/bin/sh 
nodes=$(ansible hypriotOS --list-hosts 2>/dev/null | grep -v hosts|sed "s/  //g")
tmux new-session 'bash'
for i in $nodes; do
  tmux split-window -v 'ssh pirate@$i ~/tmux-info.sh'
done
tmux select-layout even-vertical
tmux -2 attach-session -d
