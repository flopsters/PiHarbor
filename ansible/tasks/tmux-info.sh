#!/bin/sh 
tmux new-session -d 'while true;do clear;echo $HOSTNAME|toilet --gay;date;sleep 9;done'
tmux split-window -v 'docker stats --format "table{{.Name}}\t{{.CPUPerc}}\t{{.MemUsage}}"'
tmux split-window -v 'sudo journalctl -xef'
tmux split-window -v 'bash'
tmux select-layout even-vertical
tmux -2 attach-session -d
